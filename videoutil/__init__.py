import argparse


# ARGUMENT PARSER
description = """
use videoutil to play and record video with annotations
"""

parser = argparse.ArgumentParser(description=description)

parser.add_argument("-r", "--record",
                    help="record a new video with the annotations included",
                    action="store_true")
parser.add_argument('file',
                    type=str,
                    nargs='*',
                    default='',
                    help="to play (or record) the specific file")

arg = parser.parse_args()