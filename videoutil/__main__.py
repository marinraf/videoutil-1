import cv2
import numpy as np
from videoutil import arg


def main():
    space = 32
    esc = 27

    record = arg.record
    pause = False

    try:
        path_video_in = arg.file[0]
    except:
        print('please select the file you want to use')
        return

    path_video_out = path_video_in[:-4] + '_converted.avi'
    path_npz = path_video_in[:-4] + '.npz'

    try:
        data = np.load(path_npz, allow_pickle=True)
    except:
        print('npy file not found', path_npz)
        return

    timestamps = data['a']
    frames = data['b']
    states = data['c']
    list_texts = data['d']
    list_circles0 = data['e']
    list_circles1 = data['f']

    texts = []
    circles0 = []
    circles1 = []
    times = timestamps[0] - timestamps[0][0]

    j = 0
    for i in range(len(timestamps)):
        if i == frames[j]:
            text = list_texts[states[j]]
            circle0 = list_circles0[states[j]]
            circle1 = list_circles1[states[j]]
        else:
            text = ''
            circle0 = 1000
            circle1 = 1000
        texts.append(text)
        circles0.append(circle0)
        circles1.append(circle1)

    video = cv2.VideoCapture(path_video_in)

    fps = int(video.get(cv2.CAP_PROP_FPS))
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fourcc = int(video.get(cv2.CAP_PROP_FOURCC))
    fourcc = "".join([chr((fourcc >> 8 * i) & 0xFF) for i in range(4)])
    fourcc_out = cv2.VideoWriter_fourcc(*fourcc)

    out = cv2.VideoWriter(path_video_out, fourcc_out, fps, (width, height))

    total_frames = video.get(cv2.CAP_PROP_FRAME_COUNT)

    i = 0
    while video.isOpened():
        ret, frame = video.read()
        if ret:
            cv2.putText(frame, str(times[i])[:7], (450, 15), cv2.FONT_HERSHEY_DUPLEX,
                        0.5, (255, 255, 255), 1, cv2.LINE_AA)
            if texts[i] != '':
                cv2.circle(frame, (circles0[i], circles1[i]), 12, (255, 255, 255), -1)
                cv2.putText(frame, texts[i], (450, 35), cv2.FONT_HERSHEY_DUPLEX,
                            0.5, (255, 255, 255), 1, cv2.LINE_AA)
            if record:
                out.write(frame)
                i += 1
            else:
                cv2.imshow('frame', frame)
                if not pause:
                    i += 1
                k = cv2.waitKey(1) & 0xff
                if k == esc:
                    break
                if k == ord('x'):
                    i = max(int(video.get(cv2.CAP_PROP_POS_FRAMES)) - 1200, 0)
                    if i >= total_frames:
                        break
                    video.set(cv2.CAP_PROP_POS_FRAMES, i)
                if k == ord('c'):
                    i = max(int(video.get(cv2.CAP_PROP_POS_FRAMES)) + 1200, 0)
                    if i >= total_frames:
                        break
                    video.set(cv2.CAP_PROP_POS_FRAMES, i)
                if k == ord('z'):
                    i = max(int(video.get(cv2.CAP_PROP_POS_FRAMES)) - 12000, 0)
                    if i >= total_frames:
                        break
                    video.set(cv2.CAP_PROP_POS_FRAMES, i)
                if k == ord('v'):
                    i = max(int(video.get(cv2.CAP_PROP_POS_FRAMES)) + 12000, 0)
                    if i >= total_frames:
                        break
                    video.set(cv2.CAP_PROP_POS_FRAMES, i)
                if k == space:
                    pause = not pause
        else:
            break

    video.release()
    out.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
