import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="videoutil",
    version="0.0.1",
    author="Rafael Marín",
    author_email="marinraf@gmail.com",
    description="Package to play and record video with annotations",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://delaRochaLab@bitbucket.org/delaRochaLab/videoutil.git",
    install_requires=[],
    include_package_data=True,
    packages=setuptools.find_packages(),
    data_files=[
        ('', ['settings.ini']),
    ],
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points={
        'console_scripts': ['videoutil=videoutil.__main__:main']
    }
)
